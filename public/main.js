/* global fetch */

// Our main function
const main = () => {
	// Grab the list element
	const list = document.querySelector('ul');
	// Grab the content loader button
	const contentLoadButton = document.querySelector('#content-loader-button');

	// The ajax function
	const fetchHeroes = () => {
		// Use the new js fetch api to call our api endpoint
		fetch('/api/heroes')
			.then(response => response.json())
			.then((response) => {
				// And when it gets a response, clear the current list
				list.innerHTML = '';
				// And then loop through each of the members
				response.forEach((hero) => {
					// Adding their name to the list
					list.innerHTML += `<li>${hero.identity}: ${hero.name}</li>`;
				});

				// Finally, delete the button from the page now that it's not needed
				contentLoadButton.parentNode.removeChild(contentLoadButton);
			});
	};

	// Tell the button to run the fetch function when clicked
	contentLoadButton.addEventListener('click', fetchHeroes);
};

// A function that takes in a function as a parameter, and runs that function
// when the page has fully loaded.
const ready = (fn) => {
	if (document.attachEvent ? document.readyState === 'complete' : document.readyState !== 'loading') {
		fn();
	} else {
		document.addEventListener('DOMContentLoaded', fn);
	}
};

// So, when ready, run the main function
ready(main);
