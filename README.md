# Simple JS App
This is a simple Express app that serves up static files and has one 'api endpoint' that returns some JSON.

## Code Descriptions
For the server-side code, look at [index.js](https://gitlab.com/gabeotisbenson/simple-js-app/blob/master/index.js).
For the client-side code, look in the [public folder](https://gitlab.com/gabeotisbenson/simple-js-app/tree/master/public).

## Dependencies
Before you can run the application, you'll need to install Node.js and NPM.  Use the link to do so: [get Node.js](https://nodejs.org/en/).

## Running
- First, clone this repository: `git clone https://gitlab.com/gabeotisbenson/simple-js-app.git`
- Then, enter the folder you've cloned it into: `cd simple-js-app`
- Install needed dependencies: `npm i`
- Start the application: `node index.js`
- Then, in a browser, navigate to [localhost:3000](localhost:3000) and follow the instructions on the page.
