// Pull in required modules
const express = require('express');	// Web Server framework
const path = require('path');		// Filepath module

// Create our server application
const app = express();

// The path to our static assets folder ('public').
// __dirname is a global var that resolves to the current working directory
const staticPath = path.join(__dirname, 'public');

// Tell express to serve up files from our static folder
app.use(express.static(staticPath));

// This function would call a database, but since we're just doing a dummy
// example, it just returns an array created in memory
const getHeroes = () => {
	const heroes = [
		{ name: 'Spider-Man', identity: 'Peter Parker' },
		{ name: 'Batman', identity: 'Bruce Wayne' },
		{ name: 'Iron Man', identity: 'Tony Stark' },
		{ name: 'Hulk', identity: 'Bruce Banner' }
	];

	return heroes;
};

// When a request comes in for /api/members
app.get('/api/heroes', (req, res) => {
	// Call our DB fetcher function to get a list of heroes
	const heroes = getHeroes();
	// And send that back to the user
	res.send(heroes);
});

// Start listening for web requests on the specified port
const port = 3000;
app.listen(port, () => {
	console.log(`Application now listening on port ${port}`);
});
